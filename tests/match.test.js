const {getMatch} = require('../utils/match');

describe("getMatch Test", () => {
    test("should return an empty array if an empty array is given", () => {
        expect(getMatch([], [])).toStrictEqual([])    
    })
    
    test("should return no unmatched", () => {
        let buyer = [{"name": "Rolando", "price": 100, "orderType": "Buy"}]
        let seller = [{"name": "Rolando", "price": 100, "orderType": "Sell"}]
        expect(getMatch(buyer, seller)[1]).toEqual([])    
    })

    test("should return one match", () => {
        let buyer = [{"name": "Rolando", "price": 100, "orderType": "Buy"}]
        let seller = [{"name": "Rolando", "price": 100, "orderType": "Sell"}]
        expect(getMatch(buyer, seller)[0][0].buyer.name).toEqual("Rolando")    
    })
})
