const { Order } = require('../models/order');

// Ensure order table exists
const initializeOrderTable = async() => {
    try {
        await Order.sync({force: true})
        await Order.create({})
    } catch (err) {
        console.error(err)
    }
}

// Handler for creating an order
const createOrder = async(order) => {
    try {
        console.log(order)
        const createdOrder = await Order.create(order);
        await createdOrder.save();
        return order
    } catch(err) {
        console.error(err)
    }
};

// Handler for retrieving orders
const retrieveOrders = async() => {
    try {
        let orders = await Order.findAll({ raw: true })
        return orders
    } catch(err) {
        console.error(err)
    };
};

// Handler for delete all orders
const deleteOrders = async() => {
    try {
        let orders = await Order.destroy({
            where: {},
            truncate: true
        })
        return orders
    } catch(err) {
        console.error(err)
    };
};

// Handler for retrieving orders
const retrieveOrdersByType = async(type) => {
    try {
        let orders = await Order.findAll({ raw: true, where: { orderType: type }, order: [["price", "DESC"]]})
        return orders
    } catch(err) {
        console.error(err)
    };
};

module.exports = { initializeOrderTable, createOrder, retrieveOrders, deleteOrders, retrieveOrdersByType }