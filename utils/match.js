function getMatch(buyOrders, sellOrders) {
    // Ensure at least one order is available by orderType
    if (buyOrders.length < 1 || sellOrders.length < 1) {
        return []
    }
    // Create matched orders array and iterate over buy orders
    let matchedOrders = [];
    let unmatchedOrders = [];
    // This is when the prices are the same
    buyOrders.map((item) => {
        let matchedElement = sellOrders.findIndex((element) => element.price === item.price)
        if (matchedElement !== -1) {
            matchedOrders.push({"buyer": item, "seller": sellOrders[matchedElement]})
            sellOrders.splice(matchedElement, 1)
        } else {
            unmatchedOrders.push(item)
        }
    })
    // TODO: Add n:m matching pricing. Example: Rolando and Raj as buyers sum 250 and Jill selling 250
    // TODO: Add priority to sellers or buyers. Example: Rolando asks for 100, and Jill offers 200. If Jill wants, we can make divide in two orders to match Rolando's buy order.
    unmatchedOrders = unmatchedOrders.concat(sellOrders)
    return [matchedOrders, unmatchedOrders]
}

module.exports = { getMatch }