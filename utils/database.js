const { Sequelize } = require('sequelize');

const db = {}
db.sequelize = new Sequelize(process.env.DB_STRING || "postgres://username:password@localhost:5432/test");

const testDatabaseConnection = async () => {
    try {
        await db.sequelize.authenticate()
        return "Authenticated";
    } catch (err) {
        return "Failed to authenticate";
    }
};

module.exports = { testDatabaseConnection, db };