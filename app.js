const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const compression = require('compression');
const bodyParser = require('body-parser');
const { createOrder, initializeOrderTable, retrieveOrders, deleteOrders, retrieveOrdersByType } = require('./handlers/order');
const { getMatch } = require('./utils/match');

// Main configuration settings
const app = express();
const port = 3000;
app.use(morgan(process.env.NODE_ENV === 'production' ? 'tiny' : 'dev'));
app.use(cors());
app.use(compression());
app.use(bodyParser.json({ type: 'application/json' }));

// TODO: Move this to routes file
// Health check using smoke test
app.get('/health', (req, res) => {
    res.status(200).json({ "status": "OK" });
});

// Input service using specification for buyers and sellers
app.post("/orders/placement", async (req, res) => {
    try {
        let order = req.body;
        let orderCreated = await createOrder(order);
        res.status(200).json({ "status": "Item added", "data": orderCreated });
    } catch (err) {
        res.status(500).json({ "status": "Error creating order", "data": err });
    }
});

// Get all orders placed in the database
app.get("/orders/", async (req, res) => {
    try {
        let orders = await retrieveOrders();
        res.status(200).json({ "status": "Retrieving data", "data": orders });
    } catch (err) {
        res.status(500).json({ "status": "Error retrieving data", "data": err });
    }
});

// Match orders service
app.get("/orders/match", async (req, res) => {
    try {
        // Get orders by orderType
        let buyOrders = await retrieveOrdersByType("Buy");
        let sellOrders = await retrieveOrdersByType("Sell");
        let [matchedOrders, unmatchedOrders] = await getMatch(buyOrders, sellOrders)
        if (!matchedOrders || !unmatchedOrders) {
            res.status(200).json({ "status": "Not enough data to match", "data": [] });
            return
        }
        res.status(200).json({ "status": "Retrieving data", "data": {"matched": matchedOrders, "unmatched": unmatchedOrders} });
    } catch (err) {
        res.status(500).json({ "status": "Error retrieving data", "data": err });
    }
});

// Clean up registers in the database
// This is for testing purposes only, not for production
app.delete("/orders/", async (req, res) => {
    try {
        let orders = await deleteOrders()
        res.status(200).json({ "status": "Deleting data...", "data": orders });
    } catch (err) {
        res.status(500).json({ "status": "Error deleting orders", "data": err });
    }
})

// App service initialization
app.listen(port, () => {
    // Second smoke test, ensure database is working properly
    //testDatabaseConnection()
    // Uncomment for database force creation
    //initializeOrderTable()
    console.log(process.env.NODE_ENV)
    console.log("Listening on port " + port);
});

module.exports = { app }