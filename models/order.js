const { db } = require('../utils/database');
const { DataTypes } = require('sequelize');

// Order model declaration
const Order = db.sequelize.define('Order', {
    name: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: 'Joe'
    },
    orderType: {
        type: DataTypes.STRING,
        defaultValue: 'Buy'
    },
    price: {
        type: DataTypes.DOUBLE,
        defaultValue: 0
    }
})

module.exports = { Order }