dev:
	npm run start:dev
prod:
	npm run start:prod
test:
	npm run test
build:
	docker build -t bonde-back .
run-build:
	. ./setup.sh